from django.shortcuts import render

# Create your views here.
def story2(request):
    return render(request, 'story2.html')

def myTop3(request):
    return render(request, 'myTop3.html')