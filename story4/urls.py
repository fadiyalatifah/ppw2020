from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.story2, name='story2'),
    # path('story2/', views.story2, name='story2'),
    path('myTop3/', views.myTop3, name='myTop3'),
    # dilanjutkan ...
]