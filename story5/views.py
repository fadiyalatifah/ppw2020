from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .models import MataKuliah
from .forms import formMatkul


# Create your views here.
def formStory5(request):
    data = MataKuliah.objects.all()
    response = {
        'form_tambah': formMatkul(),
        'data': data,
    }
    return render(request, 'story5.html', response)

    
def postStory5(request):
    if request.method == 'POST':
        form = formMatkul(request.POST or None, request.FILES or None)
        response_data = {}
        if form.is_valid():
            response_data['nama'] = request.POST['nama']
            response_data['dosen'] = request.POST['dosen']
            response_data['jumlahSks'] = request.POST['jumlahSks']
            response_data['deskripsi'] = request.POST['deskripsi']
            response_data['semesterTahun'] = request.POST['semesterTahun']
            response_data['ruangKelas'] = request.POST['ruangKelas']

            dataForm = MataKuliah(nama=response_data['nama'],
                                 dosen=response_data['dosen'],
                                 jumlahSks=response_data['jumlahSks'],
                                 deskripsi=response_data['deskripsi'],
                                 semesterTahun=response_data['semesterTahun'],
                                 ruangKelas=response_data['ruangKelas'],
                                 )
            dataForm.save()
            return redirect('/story5/#tambahJadwal')

        else:
            return redirect('/story5')
    else:
        return redirect('/story5')





def detailJadwal(request, pk_matkul):
    data = MataKuliah.objects.filter(nama=pk_matkul)
    response = {
        'data': data,
    }
    return render(request, 'story5details.html', response)


def deleteJadwal(request, pk_matkul):
    data = MataKuliah.objects.filter(nama=pk_matkul)
    data.delete()
    return redirect('/story5')
