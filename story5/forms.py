from django import forms
from .models import MataKuliah


class formMatkul(forms.Form):

    class Meta:
        model = MataKuliah
        fields = ('__all__')

    CATEGORY = (
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2021/2022', 'Gasal 2021/2022'),
        ('Genap 2021/2022', 'Genap 2021/2022'),
        
        
    )
    SKS = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
    )

    attrs = {'class': 'form-control'}
    nama = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Mata Kuliah ', max_length=100, required=True)
    dosen = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Nama Dosen ', max_length=100, required=True)
    jumlahSks = forms.IntegerField(widget=forms.Select(choices=SKS,
        attrs=attrs), label='Jumlah SKS ', required=True)
    deskripsi = forms.CharField(widget=forms.Textarea(
        attrs=attrs), label='Deskripsi ',max_length=2000, required=True)
    semesterTahun = forms.CharField(widget=forms.Select(
        choices=CATEGORY, attrs=attrs), label='Tahun Ajar ', max_length=100, required=True)
    ruangKelas = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Ruang Kelas ', max_length=100, required=True)