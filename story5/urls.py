from django.urls import path
from . import views


urlpatterns = [
    path('', views.formStory5),
    path('post', views.postStory5),
    path('delete/<str:pk_matkul>', views.deleteJadwal),
    path('<str:pk_matkul>', views.detailJadwal),
]
