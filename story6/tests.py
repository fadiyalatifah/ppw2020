from django.test import TestCase, Client
from django.urls import resolve

from .models import Kegiatan, Pendaftar
from . import views

class UnitTestForStory6(TestCase):
    def test_main_page(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_template_story6(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_eksistensi_judul(self):
        response = Client().get('/story6/')
        content = response.content.decode('utf8')
        self.assertIn("Daftar Kegiatan", content)
        self.assertIn("Nama Kegiatan:", content)
        self.assertIn("Tambah Kegiatan", content)

    def test_story6_tambah_kegiatan_valid(self):
        Client().post('/story6/submit-kegiatan', data={'nama_kegiatan': 'main among us'})
        jumlah = Kegiatan.objects.filter(nama='main among us').count()
        self.assertEqual(jumlah, 1)

    def test_story6_tambah_kegiatan_invalid(self):
        Client().post('/story6/submit-kegiatan', data={})
        jumlah = Kegiatan.objects.filter(nama='nongki di kutek').count()
        self.assertEqual(jumlah, 0)

    def test_story6_tambah_pendaftar_valid(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        Client().post('/story6/submit-pendaftar/' + str(obj.id), data={'nama_pendaftar': 'June'})
        jumlah = Pendaftar.objects.filter(nama='June').count()
        self.assertEqual(jumlah, 1)
    
    def test_story6_tambah_pendaftar_invalid_no(self):
        Client().post('/story6/submit-pendaftar', data={})
        jumlah = Pendaftar.objects.filter(nama='Donald Trump').count()
        self.assertEqual(jumlah, 0)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='rebahan')
        kegiatan = Kegiatan.objects.get(nama='rebahan')
        self.assertEqual(str(kegiatan), 'rebahan')

    def test_story6_model_pendaftar(self):
        Pendaftar.objects.create(nama='mang oleh')
        pendaftar = Pendaftar.objects.get(nama='mang oleh')
        self.assertEqual(str(pendaftar), 'mang oleh')

    def test_func_page(self):
        test = resolve('/story6/')
        self.assertEqual(test.func, views.story6)

#halooo

# # Template testcases
#     def test_template_story6_using_index_template(self):
#         response = Client().get('/story6/')
#         self.assertTemplateUsed(response, 'story6/index.html')  

#     def test_template_story6_can_save_aktivitas_POST_request(self):
#         response = Client().post('/story6/tambah/', 
#             data = {
#                 'nama': 'Mengerjakan TK'
#             }
#         )
#         count_all_available_aktivitas = Aktivitas.objects.all().count()
#         self.assertEqual(count_all_available_aktivitas, 1)

#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')

#         response = Client().get('/story6/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('Mengerjakan TK', html_response)

#     def test_template_story6_can_delete_aktivitas_from_POST_request(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         id = aktivitas_baru.id
#         response = Client().post(
#             '/story6/hapus/' + str(id) + '/',
#         )
        
#         count_all_available_aktivitas = Aktivitas.objects.all().count()
#         self.assertEqual(count_all_available_aktivitas, 0)
#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')

#     def test_template_story6_cannot_delete_aktivitas_from_GET_request(self):
#         response = Client().get(
#             '/story6/hapus/0/',
#         )
#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')  

#     def test_template_story6_can_save_peserta_from_POST_request(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         id = aktivitas_baru.id
#         response = Client().post(
#             '/story6/peserta/tambah/', 
#             data = {
#                 'nama': 'Muhammad Alif Saddid',
#                 'aktivitas': id,
#             }
#         )

#         count_all_available_peserta = Peserta.objects.all().count()
#         self.assertEqual(count_all_available_peserta, 1)

#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')

#         response = Client().get('/story6/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('Muhammad Alif Saddid', html_response)
    
#     def test_template_story6_can_delete_peserta_from_POST_request(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
#         id = peserta_baru.id
#         response = Client().post(
#             '/story6/peserta/hapus/' + str(id) + '/',
#         )
        
#         count_all_available_peserta = Peserta.objects.all().count()
#         self.assertEqual(count_all_available_peserta, 0)
#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')

#     def test_template_story6_cannot_delete_peserta_from_GET_request(self):
#         response = Client().get(
#             '/story6/peserta/hapus/0/',
#         )
#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/story6/#index')

#     # Model testcases
#     def test_model_aktivitas_create(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')

#         count_all_available_aktivitas = Aktivitas.objects.all().count()
#         self.assertEqual(count_all_available_aktivitas, 1)

#     def test_model_peserta_create(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)

#         count_all_available_peserta = Peserta.objects.all().count()
#         self.assertEqual(count_all_available_peserta, 1)

#     def test_model_aktivitas_delete(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         aktivitas_baru.delete()

#         count_all_available_aktivitas = Aktivitas.objects.all().count()
#         self.assertEqual(count_all_available_aktivitas, 0)

#     def test_model_peserta_delete(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
#         peserta_baru.delete()

#         count_all_available_peserta = Peserta.objects.all().count()
#         self.assertEqual(count_all_available_peserta, 0)

#     def test_model_peserta_delete_cascade(self):
#         aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
#         peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
#         aktivitas_baru.delete()

#         count_all_available_aktivitas = Aktivitas.objects.all().count()
#         count_all_available_peserta = Peserta.objects.all().count()
#         self.assertEqual(count_all_available_peserta, 0)
#         self.assertEqual(count_all_available_aktivitas, 0)