from django.db import models


# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama


class Pendaftar(models.Model):
    nama = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.nama
