from django import forms

class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis nama kegiatan disini'}))

class FormPendaftar(forms.Form):
    nama_pendaftar = forms.CharField(label='Mau ikut? daftarkan dirimu', max_length=50,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis namamu disini'}))
