from django.contrib import admin

# Register your models here.
from .models import Kegiatan, Pendaftar

admin.site.register(Kegiatan)
admin.site.register(Pendaftar)
