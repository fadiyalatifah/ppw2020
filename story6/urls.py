from django.urls import path
from . import views


urlpatterns = [
    path('', views.story6),
    path('submit-kegiatan', views.submit_kegiatan),
    path('submit-pendaftar/<int:pk>', views.submit_pendaftar),
    path('delete/<str:pk>', views.deleteKegiatan),
]
